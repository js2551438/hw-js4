// Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера два числа.
// Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
// Вивести у консоль результат виконання функції.

let firstNumber = +prompt('Введіть перше число', 0)
let secondNumber = +prompt('Введіть друге число', 0)

if(Number.isNaN(firstNumber) || Number.isNaN(secondNumber)){
    firstNumber = +prompt('Введіть перше число')
    secondNumber = +prompt('Введіть друге число')
}

function count (firstNumber, secondNumber, method){
    switch(method) {
        case '+': {
            return firstNumber + secondNumber
        }
        case '-' : {
            return firstNumber - secondNumber
        }
        case '*' : {
            return firstNumber * secondNumber
        }
        case '/' : {
            if (secondNumber !== 0) {
                return firstNumber / secondNumber
            } else {
                return 'не ділиться на 0'
            }
        }
    }
}

console.log(count(firstNumber,secondNumber, '+'))
console.log(count(firstNumber,secondNumber, '-'))
console.log(count(firstNumber,secondNumber, '*'))
console.log(count(firstNumber,secondNumber, '/'))


// function calc (firstNumber, secondNumber, method){
//     const result = 
// }

// function calcAddition () {
//     const addition = firstNumber + secondNumber;
//     return addition
// }

// function calcSubtraction () {
//     const subtraction = firstNumber - secondNumber;
//     return subtraction
// }

// function calcMultiplication () {
//     const multiplication = firstNumber * secondNumber;
//     return multiplication
// }

// function calcDivision () {
//     if (secondNumber !== 0) {
//         return firstNumber / secondNumber
//     } else {
//         return 'не ділиться на 0'
//     }
// }
 
// console.log(calcAddition(firstNumber, secondNumber))
// console.log(calcSubtraction(firstNumber, secondNumber))
// console.log(calcMultiplication(firstNumber, secondNumber))
// console.log(calcDivision(firstNumber, secondNumber))
